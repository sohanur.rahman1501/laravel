<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function ShowHome ()
    {
        return view('admin.home');
    }

    public function login ()
    {
        return view('components.frontend.layout.partials.login');
    }

    public function classicfilter ()
    {
        return view('category.classicfilter');
    }


    
}
